import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'km-recap',
  templateUrl: './recap.component.html',
  styleUrls: ['./recap.component.scss']
})
export class RecapComponent implements OnInit {

  constructor(private router: Router) {
    this.router.getCurrentNavigation()?.extras.state;
  }

  ngOnInit(): void {
    this.comeBackHome();
    console.log(this.effyHelp)
  }

  get recap(){ return history.state.result; }
  get cost() { return this.recap['surface'] * 80; }
  get effyHelp() {return Math.floor(((this.cost * 0.75) - ((this.recap['income'] * 1000) / this.recap['resident'])) * 0.15)}

  isEligible() {
    if (this.recap)
      return !!this.recap['owner'];
    else
      return false;
  }

  //Issue with guard
  comeBackHome() {
    if (!history.state.result){
      this.router.navigateByUrl('/').then();
    }
  }



}
