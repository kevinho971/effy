import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {RecapComponent} from "./pages/recap/recap.component";
import {CanActivateGuard} from "./guards/can-activate.guard";

const routes: Routes = [
  {path: 'recap', component: RecapComponent},
  {path: '**', component: HomeComponent, redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
