import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'km-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      gender: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      firstname: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required, Validators.pattern('')])
    });
    this.secondFormGroup = this._formBuilder.group({
      owner: new FormControl('', Validators.required),
      resident: new FormControl(1, Validators.required),
      income: new FormControl(10, [Validators.required, Validators.min(10), Validators.max(100)]),
      surface: new FormControl('', Validators.required),
    });
  }

  get email() { return this.firstFormGroup.get('email'); }
  get income() { return this.secondFormGroup.get('income'); }

  goToRecap() {
    const result = {
      gender: this.firstFormGroup.value['gender'],
      email: this.firstFormGroup.value['email'],
      firstname: this.firstFormGroup.value['firstname'],
      lastname: this.firstFormGroup.value['lastname'],
      phone: this.firstFormGroup.value['phone'],
      owner: this.secondFormGroup.value['owner'],
      resident: this.secondFormGroup.value['resident'],
      income: this.secondFormGroup.value['income'],
      surface: this.secondFormGroup.value['surface'],
    }
    console.log(result);
    this.router.navigateByUrl('/recap', {state: {result}});
  }

}
